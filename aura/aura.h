/*
 * aura.h
 *
 *  Created on: 25 ���. 2019 �.
 *      Author: almaz
 */

#ifndef AURA_H_
#define AURA_H_

#include <limits.h>

#define UNITY_TEST

#ifdef UNITY_TEST
#include <unity.h>
void aura_unityTest(void);
#endif

int aura_getNumber(void);
void aura_incrementNumber(void);
int aura_setMaximumValue(int maximumValue);


#endif /* AURA_H_ */
