/*
 * aura.c
 *
 *********************************************************************************
 *  Created on: 25 ���. 2019 �.
 *      Author: almaz
 *********************************************************************************
 *  This module spins around just one parameter (number) you can modify and return.
 *  Here we call it as main parameter (MP).
 *  You can get current value of parameter using getNumber() function.
 *  The only way you can modify current value is incrementing it using incrementNumber() function.
 *  You can constraint maximum value by setMaximumValue() function.
 *
 */


#include <aura.h>

// default value of main parameter is zero.
static int magicNumber = 0;

// we desided that by default max constraint for main parameter is 0xFF.
static int maxConstraint = INT_MAX;

/**
 * @brief  Access function (Getter method). Returns current value of
 *
 * @return magicNumber
 *   Curent value of main parameter.
 */
int aura_getNumber(void)
{
	return magicNumber;
}

/**
 * @brief  Setter method. Increments main parameter.
 *
 */
void aura_incrementNumber(void)
{
	magicNumber++;
	if(magicNumber >= maxConstraint) // defaulting to zero on overflow.
		magicNumber = 0;
}

/**
 * @brief  Constraint method. Limits maximum available value that main parameter can reach.
 *   if current value higher than maximum value we want set it defaults to zero.
 *   You can't set maximum value less that zero.
 *
 * @param adr
 *   ����� �� ����-������, �� �������� ������������ ����������.
 *
 * @return flashAddress
 *   ��������� �� ����� 32-������ ��������.
 */
int aura_setMaximumValue(int maximumValue)
{
	if(maximumValue < 0) // error. You can't set maximumValue less that zero.
		return 0;

	maxConstraint = maximumValue;
	if(magicNumber >= maxConstraint)
		magicNumber = 0;

	return 1;
}

void aura_testSetMaximumValue(void)
{
	TEST_ASSERT_EQUAL_INT(1, aura_setMaximumValue(0));	// we can set constraint to zero
	TEST_ASSERT_EQUAL_INT(0, aura_setMaximumValue(-1));	// we can't set constraint less than zero
	TEST_ASSERT_EQUAL_INT(1, aura_setMaximumValue(INT_MAX));	// we can set constraint to any value higher than zero
	TEST_ASSERT_EQUAL_INT(0, aura_setMaximumValue(INT_MIN));	// we can't set constraint less than zero
}

void aura_testIncrementValue(void)
{
	magicNumber = 0; //tricky dirty starting point
	TEST_ASSERT_EQUAL_INT(1, aura_setMaximumValue(5));
	TEST_ASSERT_EQUAL_INT(0, aura_getNumber());
	aura_incrementNumber();
	TEST_ASSERT_EQUAL_INT(1, aura_getNumber());
	aura_incrementNumber();
	TEST_ASSERT_EQUAL_INT(2, aura_getNumber());
	aura_incrementNumber();
	TEST_ASSERT_EQUAL_INT(3, aura_getNumber());
	aura_incrementNumber();
	TEST_ASSERT_EQUAL_INT(4, aura_getNumber());
	aura_incrementNumber();
	TEST_ASSERT_EQUAL_INT(0, aura_getNumber());
	aura_incrementNumber();
	TEST_ASSERT_EQUAL_INT(1, aura_getNumber());
}

void aura_unityTest(void) // used library - http://www.throwtheswitch.org/unity
{
	UnityBegin("aura.c");
	RUN_TEST(aura_testSetMaximumValue, 70);
	RUN_TEST(aura_testIncrementValue, 78);
	UnityEnd();
}
