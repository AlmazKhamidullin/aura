# Тестовое задание "АУРА Девайсез" на должность Программиста микроконтроллеров

# Описание задания
[Embedded software developer](https://docs.google.com/document/d/1ddCwOMI1wJBcXjQjyma9WgoaTL1TrbpGUXABOvPt3ek/edit)

# Пояснения к коду
Программный код отлажен на реальном железе, - на отладочной плате STM32.
Юнит тесты реализованы на базе библиотеки [Unity](http://www.throwtheswitch.org/unity)
Модуль расположен в отдельном каталоге `aura`. Названия функций начинаются с приставки `aura_`.

![1](doc/1.jpg)

![2](doc/3.jpg)
